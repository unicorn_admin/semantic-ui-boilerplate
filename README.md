# Simplified Boilerplate for Semantic UI

A simplified asset pipeline for Semantic UI. Built from [Semantic UI LESS](https://github.com/Semantic-Org/Semantic-UI-LESS) and a customized subset of the [Semantic Build Tasks](https://github.com/Semantic-Org/Semantic-UI/tree/master/tasks#tasks)

## Installation

Clone this repo into your new project:

```sh
git clone git@bitbucket.org:unicorn_admin/semantic-ui-boilerplate.git
```

## File Structure
```
semantic-ui-starter
 +-- dist  
 |    +-- semantic    
 +-- source  
 |    +-- semantic    
 +-- tasks            
 +-- semantic.json    
```

**dist/semantic** - default location of generated semantic assets  
**source/semantic** - default location of source files  
**tasks** - customized version of [Semantic UI Build tasks](http://semantic-ui.com/introduction/build-tools.html)  
**semantic.json** - customized version of [Semantic UI config file](http://semantic-ui.com/introduction/build-tools.html#semanticjson)  

## Usage

To build the Semantic UI assets:

```sh
gulp
```

To watch the Semantic UI assets:

```sh
gulp watch
```

## Configuration

The same as [semantic.json](http://semantic-ui.com/introduction/build-tools.html#semanticjson) with a few additional configuration options described below:

### Custom filename for generated css and js file
```js
{
  ...
  "filenames": {
    // generated assets can be named and are saved to 'output.packaged'
    "concatenatedCSS": "app.css",
    "concatenatedJS": "app.js",
    "concatenatedMinifiedCSS": "app.min.css",
    "concatenatedMinifiedJS": "app.min.js"
  }
  ...
}
```

### Override Semantic UI's default evergreen browser support
Since the gulp settings are hard-coded within Semantic UI's build tasks, I've externalized the [autoprefixer](https://github.com/postcss/autoprefixer) settings so I can keep using their build tools but have more control on what is generated.  
```js
{
  ...
  "autoprefixSettings": {
    "browsers": [
      "last 4 versions",
      "> 1%",
      "opera 12.1",
      "bb 10",
      "android 4"
    ]
  }
  ...
}
```

## Todo

* ~~Integrate watcher tasks~~
* make fonts & images output more configurable
* bootstrap jquery.js into generated assets
