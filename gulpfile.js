var gulp = require('gulp');

// Build semantic-ui
var buildSemantic = require('./tasks/build');
var watchSemantic = require('./tasks/watch');

// ### Semantic UI Initialize
// One time copy of Semantic UI files when you init a new theme
gulp.task('copySemantic', function() {
    gulp.src(['./node_modules/semantic-ui-less/**/*', '!*.{json,.md}'])
        .pipe(gulp.dest('./source/semantic'));
});

// ### Semantic UI Build
// `gulp build` - Build Semantic UI assets, css & js files
gulp.task('build', buildSemantic);

// ### Semantic UI Watch
// `gulp watch` - Watch Semantic UI assets, css & js files
gulp.task('watch', watchSemantic);

// ### Gulp
// `gulp` - Run a complete build. To compile for production run `gulp --production`.
gulp.task('default', ['build'], function() {
});